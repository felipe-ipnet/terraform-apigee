# define GCP project name
variable "gcp_project" {
  type        = string
  description = "GCP project name"
}